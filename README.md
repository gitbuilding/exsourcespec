# ExSourceSpec - A specification for a file that describes the relationship between hardware exchange files and their source

## Why do we need this?

Open Source Hardware projects often consist of a large number of original design files (or CAD files), these might be FreeCAD files, OpenSCAD files, or files from any other open or proprietary CAD package. Anyone building an open source hardware project needs either an exchange file (such as a STEP file, STL file, or DXF file) for digital manufacturing, or perhaps a PDF drawing for manual manufacturing.

For open source hardware it is essential that the original design files are shared so that others can build upon the project. However, if your project aims to make it easy to build, it is important that makers have easy access to the output files they need. This can lead to a number of issues:

1. **Output files being stored in a source repository** - As these file are often large, and generally can't be stored efficiently in revision control the repository can grow very large. Not only is this an inefficient way to store data, it can limit access in areas with limited internet access.
2. **Output files not being provided** - Technically, as the source is provided the output files can be made by anyone. Yet this raises the barrier to entry, the maker need to not only understand how to  manufacture and assemble the components, they need access to the same CAD package as you and to understand how to compile each file.
3. **Unclear link between output files and source files** - Some projects distribute output files for the source files in a given repository. These may be available on a website with instructions for how to build the hardware project. It is possible to generate the output files automatically using GitHub Actions or the GitLab CI. However, it is often not immediately clear to the maker what the source is for any given file.

This specification tries to help solve point 3 by providing clear information about output files including the corresponding source and how the file is generated. This specification is for a machine readable format, it is not expected that most makers would open this file (or even know it exists). It however can be used to present the correct data to a maker in an appropriate format for the given project.

## The Specification

The specification is a JSON schema that defines the key-value pair information that links output files to their source files. The schema also has optional fields that allow relevant information about the output file to be specified. While it is a JSON schema, the project is agnostic as to which key valued pair file format you use to store this information (YAML, TOML, JSON) as long as you can use the schema to validate the information.

How the file is named depends on how it is being used:
* The file should be named `exsource-def.yml` or `exsource-def.json` (etc for other extensions) if it is being used to describe exchange files that need to be generated from source.
* The file should be named `exsource-out.yml` or `exsource-out.json` (etc for other extensions) if it is being used to describe exchange files that have already been generated from source.

A simple example file (in YAML syntax) might look something like:

```
exports:
    large-shelf-bracket:
        name: Main Shelf Bracket
        description: >
            This is the shelf bracket that holds up the main shelf. Multiple of
            these are needed.
        output-files:
            - large-shelf-bracket.stl
        source-files:
            - shelf-brackets.scad
        parameters:
            length: 200
        application: openscad
        app-options:
            - --hardwarnings
    mini-shelf-bracket:
        name: Miniature Shelf Bracket
        description: >
            These shelve brackets go on the side of the frame, they are smaller than
            the main shelf bracket.
        output-files:
            - mini-shelf-bracket.stl
        source-files:
            - shelf-brackets.scad
        parameters:
            length: 50
        application: openscad
        app-options:
            - --hardwarnings
    frame:
        name: Main frame
        description: >
            This frame holds the shelf brackets and shelves.
        output-files:
            - frame.step
        source-files:
            - frame.FCStd
        application: freecad
```

## Uses of the specification

The file is intended to be used in two ways.

1. To be used when a project is creating its documentation to provide a clear reference for the source of each output file. The exsource file could be created by a build script that generates the output files, or it could be written manually.
2. If the exsource file is written manually it could be the input file for a build script that creates the outputs based on the specified inputs.

In some cases it might be desirable for a project to use this specification of both purposes, this might require two exsource files. The project may wish to define which output files should be created from a manually written exsource file that sits in the repository, further information generated by a build script would be then put in a more complete automatically created exsource file. The recommended naming convention would be:

* `exsource-def.yml` - The input output exsource definition
* `exsource-out.yml` - The final complete exsource file.

Both files are validated by the same schema, with optional properties being added in the final file.

## FAQ - or Questions You Might Be Thinking

### How ready is this for use?

The data contained is quite useful so there it might be useful to make an exsource file. The format will likely need some finesse to allow it to be used effectively for its intended purposes.

### Doesn't open source software have the same problem?

Yes and no.

Open source software does have source and output files that are sometimes not clearly linked. However, the final program is usually created directly from the repository. Compiling from source can be a difficult task, but it is an entirely digital that can often be entirely automated. End users get a program to use, if they are programmers they might go searching for the source.

In the case of open hardware, the final piece of hardware is not directly created from the repository. Even if the project is digitally manufactured, a human is always in the loop during the creation of the hardware. The human sources materials and other standard components,. The human loads files into the what ever machine is manufacturing them. They post process the manufactured components, and they assemble the final object. This human/maker may or may not be the final end user. To make an analogy with C compiling an executable from C code, the CAD program acts as compiler, the exchange files are the object code, the human maker is the linker that creates the final useable output. As such, in hardware it is important that the human maker has the information they need to understand a project.

Those making a hardware project are likely to need to customise something. Perhaps their manufacturing equipment has different specifications, perhaps certain components are not available locally. The question the maker is probably asking is not "Where is the source for this hardware project?" but the much more specific question of "Which file in this hardware project generated the STEP file for this bracket?"?
